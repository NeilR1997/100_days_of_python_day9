programming_dictionary = {
    "Bug": "An error in a program that prevents the program from running as expected.", 
    "Function": "A piece of code that you can easily call over and over again.",
}

#Reterives items from Dictionary
print(programming_dictionary["Function"])
print(programming_dictionary)
#Adding new items to Dictionary

programming_dictionary["Loop"] = "The action of doing something over and over again"
print(programming_dictionary)

#Wiping a Dictionary

#programming_dictionary = {}
#print(programming_dictionary)

#Edit existing items in a Dictionary
programming_dictionary["Bug"] = "A moth in your computer"
print(programming_dictionary)

#Looping through a Dictionary
for key in programming_dictionary:
    print(key) #firstly prints out the key
    # Uses the key that has been obtained within the loop to print out the value that the key has  
    print(programming_dictionary[key]) 

#Nesting


captials = {
    "United Kingdom": "Yorkshire",
    "France": "Paris",

}

#Nesting a List in a Dicitonary

travel_log = {
    "United Kingdom": ["Yorkshire", "Newcastle", "Dorset"],
}

#Nesting a Dictionary in a Dictionary
travel_log_nest = {
    "United Kingdom": {
        "Places Visited": ["Yorkshire", "Newcastle", "Dorset"],
        "Total Visits": 15
    }
}
print(travel_log_nest["United Kingdom"])


#Nesting a Dictionary in a List

travel_log_nest2 = [
{
    "Country": "United Kingdom", 
    "Places Visited": ["Yorkshire", "Newcastle", "Dorset"],
    "Total Visits": 15,
},
]
print(travel_log_nest2)