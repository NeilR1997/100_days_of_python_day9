'''
My soultion for the Day 9 Second Coding Challenge from Auditorium for adding a Dictionary to a list without modifying it from the 
Udemy Course by Dr. Angela Yu at https://www.udemy.com/course/100-days-of-code
'''

print("Please enter the country that you visited")
country = input() # Add country name
print("Enter the number of times you have visited")
visits = int(input()) # Number of visits
print("Enter the list of cities you went in surronded by a [] and each of them surronded by a ''")
list_of_cities = eval(input()) # create list from formatted string

travel_log = [
  {
    "country": "France",
    "visits": 12,
    "cities": ["Paris", "Lille", "Dijon"]
  },
  {
    "country": "Germany",
    "visits": 5,
    "cities": ["Berlin", "Hamburg", "Stuttgart"]
  },
]

def add_new_country(country, visits, list_of_cities):
  travel_log.append({
        "country": country,
        "visits":  visits,
        "cities": list_of_cities
    },)

add_new_country(country, visits, list_of_cities)
print(f"I've been to {travel_log[2]['country']} {travel_log[2]['visits']} times.")
print(f"My favourite city was {travel_log[2]['cities'][0]}.")