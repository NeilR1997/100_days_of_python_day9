import os, art
print(art.logo)
print("Welcome to the Secret Auction Program.\n")

def clear():
    command = "clear"
    if os.name in ("nt", "dos"):  # If Machine is running on Windows, use cls
        command = "cls"
    os.system(command)

bidders = []
#Loop to keep going while there is bidders remaining to add them to the bidders list
def getting_bids():
    #Empty list to store the bidders name and bid amount
   
    bidders_remaining = True

    while bidders_remaining == True:
        user_name = input("What is your name: ").lower().title() #Title function to captialize the name of the user for them and to lower caps entered by mistake
        bid_amount = int(input("What is your bid? £:"))
        #Adds the bidders Dictionary to the Bidders list
        bidders.append(
            {
                "Bidder": user_name,
                "Bid": bid_amount
            },
        )
        #To see if there is any other bids
        any_other_bids = input("Type in 'yes' if there is remaining bidders and 'no' if you were the final bidder: ").lower()
        
        #Clears the console if entered correctly else brings the prompt back up if they entered the wrong text
        entered_correctly = False
        while entered_correctly == False:    
            if any_other_bids == "yes":
                clear()
                break

            elif any_other_bids == "no":
                entered_correctly = True
                bidders_remaining = False
                break    
            else:
                print("Enter yes or no")
                any_other_bids = input("Type in 'yes' if there is remaining bidders and 'no' if you were the final bidder: ")



#Loop for checking all the bidders in the list
def the_winner():
    #Comparing the bid amounts and getting the highest bid result
    index = 0 # For iterating through the list of Dictionaries
    final_amount = 0 # Declaring the variable to hold the final amount

    for total in bidders:
            total = bidders[index]["Bid"] #Access the bid key value
            if total > final_amount:
                winner = bidders[index]["Bidder"] #Gets the name of the Winner
                final_amount = total #Sets the amount they bidded to the final amount
            # If the total that is accessed is larger than the final amount replace it
            index +=  1 #Plus 1 to the index to go through it again
    clear()
    print(f"The winner is {winner} with a bid of £{final_amount}\n {art.winner}")       

getting_bids()
the_winner()