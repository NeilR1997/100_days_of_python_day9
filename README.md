# 100 Days of Python Day 9

## Learning Objectives

- Creating an Secret Auction game where it will allow the people who are in the auction to bid not knowing the other people's bid and once all the bids have been put in it will declare the winner as the person who had the highest bid.

- We will be learning about the Python Dictionary

- Interactive Code Exercise: Grading Program

- How to nest lists and Dictionaries

- How to nest Lists within Lists, Dictionaries within Dictionaries and nesting them in each other.

- Inteactive Code Exercie: Dictionary in List
